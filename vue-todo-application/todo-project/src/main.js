import { createApp } from 'vue'
import App from './App.vue'


import "bootstrap/dist/css/bootstrap.min.css"; //importing bootstrap
import "bootstrap-icons/font/bootstrap-icons.css"; // importing bootstrap icons

createApp(App).mount('#app')
